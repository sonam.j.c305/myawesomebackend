package postgres

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
)

const (
	postgres_host     = "dpg-chrbo18rddlba9ufjjl0-a.singapore-postgres.render.com"
	postgres_port     = 5432
	postgres_user     = "postgres_admin"
	postgres_password = "eWjeP6IzcOGZDlqtvmIbxjt4Gi25Ep84"
	postgres_dbname   = "my_db_qx9p"
)

var Db *sql.DB //creating pointer variable which points to sql driver or we are refering to the db

// init is always called before main
func init() {
	//creating connection string

	db_info := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s", postgres_host, postgres_port, postgres_user, postgres_password, postgres_dbname)

	var err error
	//open connection to database
	Db, err = sql.Open("postgres", db_info)

	// error handler
	if err != nil {
		panic(err)
	} else {
		log.Println("Database successfully configured")
	}
}
