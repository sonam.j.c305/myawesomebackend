CREATE DATABASE my_db;

 CREATE TABLE student(
        StdId int not null,
         Firstname varchar(45) not null,
        Lastname varchar(45) default null,
        Email varchar(45) not null,
         PRIMARY KEY (StdId),
    UNIQUE(Email)
 )