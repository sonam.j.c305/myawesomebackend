CREATE TABLE student (
player_ID int NOT NULL,
FirstName varchar(45) NOT NULL,
LastName varchar(45) DEFAULT NULL,
kit_number varchar(45) NOT NULL,
PRIMARY KEY (player_ID),
UNIQUE (kit_number)
)