package routes

import (
	"goapp/myapp/controller"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func IntializeRoutes() {
	router := mux.NewRouter()

	//admin:
	router.HandleFunc("/admin", controller.GetAdmin).Methods("GET")

	router.HandleFunc("/admin/{cid}", controller.UpdateProfile).Methods("PUT")

	//Players:

	router.HandleFunc("/player", controller.AddPlayer).Methods("POST")

	router.HandleFunc("/player/{pid}", controller.GetPlayer).Methods("GET")

	router.HandleFunc("/players", controller.GetAllPLayers)

	router.HandleFunc("/totalplayers", controller.GetTotalPlayers).Methods("GET")

	router.HandleFunc("/player/{pid}", controller.DeletePlayer).Methods("DELETE")

	// staff:
	router.HandleFunc("/staff", controller.AddSTAFF).Methods("POST")

	router.HandleFunc("/staff/{sid}", controller.GetStaff).Methods("GET")

	router.HandleFunc("/staffs", controller.GetAllSTaffs)

	router.HandleFunc("/totalstaffs", controller.GetTotalStaffs).Methods("GET")

	router.HandleFunc("/staff/{sid}", controller.DeleteStaff).Methods("DELETE")

	//login signup
	router.HandleFunc("/signup", controller.SignUp).Methods("POST")

	router.HandleFunc("/login", controller.Login).Methods("POST")

	router.HandleFunc("/totaladmins", controller.GetTotalAdmin).Methods("GET")

	fhandler := http.FileServer(http.Dir("./index"))
	router.PathPrefix("/").Handler(fhandler)

	log.Println("Application running on port 8080...")
	log.Fatal(http.ListenAndServe(":8080", router))
}
