
window.onload = function (){
    fetch('/players', {
        method: "GET"
    })
        .then(response => response.text())
        .then(data => showPlayers(data))
}


function showPlayer(data) {
    var search_data = {
        pid : parseInt(document.getElementById("anup").value),
    }
    const player = JSON.parse(search_data)
    newRow(player)
}

function newRow(player) {
    var table = document.getElementById("myTable");
    var row = table.insertRow(table.length);
    var td = []
    for( i=0; i<table.rows[0].cells.length; i++){
        td[i] = row.insertCell(i)
    }
    td[0].innerHTML = player.pid;
    td[0].style.border = "1px solid black";
    td[0].style.textAlign = "center";

    td[1].innerHTML = player.fname;
    td[1].style.border = "1px solid black";
    td[1].style.textAlign = "center";

    td[2].innerHTML = player.lname;
    td[2].style.border = "1px solid black";
    td[2].style.textAlign = "center";

    td[3].innerHTML = player.kitnumber;
    td[3].style.border = "1px solid black";
    td[3].style.textAlign = "center";

    td[4].innerHTML = '<input type="button" onclick="deletePlayer(this)" value="del" id="button-1">';
    td[4].style.border = "1px solid black";
    td[4].style.textAlign = "center";


}

function showPlayers(data){
    const players = JSON.parse(data)
    players.forEach(play => {
        newRow(play)
    });
}


function deletePlayer(r){
    if(confirm('Are you sure you want to DELETE this ?')){
        selectedRow = r.parentElement.parentElement;
        Pid = selectedRow.cells[0].innerHTML;

        fetch('/player/'+Pid, {
            method : "DELETE",
            headers : {"Content-type": "application/json; charset=UTF-8"}
        });
        var rowIndex = selectedRow.rowIndex;
        if(rowIndex>0){
            document.getElementById("myTable").deleteRow(rowIndex);
        }
        selectedRow = null;
    }
}
