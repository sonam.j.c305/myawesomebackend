$(document).ready(function() {
    $.ajax({
        url: "/totalplayers",
        type: "GET",
        dataType: "json",
        success: function(response) {
            // $("#number-players").text(response.total);
            $("#number-players").text(response.total).css("text-align", "center");
        },
        error: function(_xhr, _status, error) {
            console.error(error);
        }
    });
});

$(document).ready(function() {
    $.ajax({
        url: "/totalstaffs",
        type: "GET",
        dataType: "json",
        success: function(response) {
            // $("#number-players").text(response.total);
            $("#number-staffs").text(response.total).css("text-align", "center");
        },
        error: function(_xhr, _status, error) {
            console.error(error);
        }
    });
});

$(document).ready(function() {
    $.ajax({
        url: "/totaladmins",
        type: "GET",
        dataType: "json",
        success: function(response) {
            $("#number-admins").text(response.total).css("text-align", "center");
        },
        error: function(_xhr, _status, error) {
            console.error(error);
        }
    });
});